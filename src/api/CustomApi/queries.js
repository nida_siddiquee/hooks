import axios from "axios";

const getCats = async () => {
  let url = "https://cat-fact.herokuapp.com/facts";
  return axios({
    method: "get",
    url: url,
  });
};

export { getCats };
