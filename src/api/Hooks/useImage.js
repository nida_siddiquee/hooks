import { useCallback, useContext, useEffect } from "react";
import NumberContext from "../../Contexts/NumberContext";
const useImage = () => {
  const value = useContext(NumberContext);
  const setImageLoaded = useCallback(
    (loaded) => {
      console.log("loaded in Callback: ", loaded);
      value.setGlobalData({ ...value.globalData, loaded });
    },
    [value]
  );
  const showData = () => {
    console.log("USeEffect called , value   ", value.globalData.loaded);
  };
  useEffect(showData, [showData]);
  return { value, setImageLoaded };
};

export default useImage;
