import { useEffect, useState } from "react";
import { getCats } from "../CustomApi/queries";

const useCat = () => {
  const [remoteData, setRemoteData] = useState(null);
  const [count, setCount] = useState(0);
  const fetchingCatFacts = () => {
    getCats()
      .then((res) => {
        if (res?.status === 200 || res?.status === 201) {
          setRemoteData(res?.data);
          console.log("Fetched Data : ", res.data);
        }
      })
      .catch((err) => {
        console.log("useCat.js Fetching Cats Api Failed : ", err);
      });
  };

  const calCount = () => {
    setCount(count + 1);
  };

  useEffect(() => {
    fetchingCatFacts();
  }, []);

  return { data: remoteData, count: count, updateCount: calCount };
};

export default useCat;
