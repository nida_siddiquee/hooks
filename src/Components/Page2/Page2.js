import React from "react";
import useImage from "../../api/Hooks/useImage";

const Page2 = () => {
  const { value, setImageLoaded } = useImage();
  console.log("Page 2", value.globalData.imgUrl2);

  return (
    <div style={{ textAlign: "center" }}>
      <h1>Page 2</h1>
      {value.globalData.loaded && (
        <img
          src={value.globalData.imgUrl2}
          onError={() => {
            console.log("loading failed:");
            setImageLoaded?.(false);
          }}
          alt=""
        />
      )}
    </div>
  );
};

export default Page2;
