import { useState } from "react";
import Home from "./Components/Home";
import NumberContext from "./Contexts/NumberContext";

function App1() {
  const [globalData, setGlobalData] = useState({
    imgUrl1:
      "https://www.a2hosting.com/blog/content/uploads/2018/07/man-holding-camera.png",
    imgUrl2:
      "https://image.shutterstock.com-beautiful-drops-transparent-rain-600w-668593321.jpg",

    imgUrl3:
      "https://image.shutterstock.com/image-photo/large-beautiful-drops-transparent-rain-600w-668593321.jpg",
    loaded: true,
  });
  return (
    <NumberContext.Provider value={{ globalData, setGlobalData }}>
      <Home />
    </NumberContext.Provider>
  );
}

export default App1;
