import React from "react";
import useImage from "../../api/Hooks/useImage";

const Page1 = () => {
  const { value, setImageLoaded } = useImage();
  console.log("Page 1", value.globalData.imgUrl1);

  return (
    <div style={{ textAlign: "center" }}>
      <h1>Page 1</h1>
      {value.globalData.loaded && (
        <img
          src={value.globalData.imgUrl1}
          onError={() => {
            console.log("loading failed:");
            setImageLoaded?.(false);
          }}
          alt=""
        />
      )}
    </div>
  );
};

export default Page1;
