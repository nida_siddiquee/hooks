import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Page1 from "../Page1";
import Page2 from "../Page2";
import useImage from "../../api/Hooks/useImage";

const Home = () => {
  const { value, setImageLoaded } = useImage();
  console.log("Home Screen", value.globalData.imgUrl3);
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/page2">Page 2</Link>
            </li>
          </ul>
        </nav>
        <div>
          <Route path="/">
            <div style={{ textAlign: "center" }}>
              <h1>Home</h1>
              {value.globalData.loaded && (
                <img
                  src={value.globalData.imgUrl3}
                  onError={() => {
                    console.log("loading failed:");
                    setImageLoaded?.(false);
                  }}
                  alt=""
                />
              )}
            </div>
          </Route>
          <Page1 />
          <Route path="/page2">
            <Page2 />
          </Route>
        </div>
      </div>
    </Router>
  );
};
export default Home;
